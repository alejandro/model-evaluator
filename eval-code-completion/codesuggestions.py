import json

from anthropiccode import complete_anthropic_code
from codeutils import Timer
from flask import Flask, request
from gitlabcodegen import complete_gitlab_native_code
from vertexaicode import (
    complete_vertex_code,
    complete_vertex_code_bison,
    complete_vertex_gecko,
    complete_vertex_text_bison,
)

token_target_creation = 256
token_target_completion = 128


def test_run(test_function, language, code, **kwargs):
    t = Timer()
    t.start()
    result_code = test_function(language, code, **kwargs)
    duration = t.stop()

    if result_code:
        return {
            "result": result_code,
            "duration": duration,
            "result_length": len(result_code),
        }
    else:
        return {"result": ""}


def _test_code_example(element):
    print(element["user_intention"])
    test_code = element["prompt"]
    element["anthropic"] = test_run(complete_anthropic_code, element["language"], test_code)
    print("\n\n")
    element["anthropic_fast"] = test_run(complete_anthropic_code, element["language"], test_code, use_fast=True)
    print("\n\n")
    element["vertex_combined"] = test_run(complete_vertex_code, element["language"], test_code)
    print("\n\n")
    element["vertex_code_bison"] = test_run(complete_vertex_code_bison, element["language"], test_code)
    print("\n\n")
    element["vertex_text_bison"] = test_run(complete_vertex_text_bison, element["language"], test_code)
    print("\n\n")
    element["vertex_gecko"] = test_run(complete_vertex_gecko, element["language"], test_code)
    print("\n\n")
    element["gitlab_model"] = test_run(complete_gitlab_native_code, element["language"], test_code)
    print(element)


def _process_request(request, runner):
    data = request.json
    print("LANGUAGE : " + data.get("language"))
    print(data.get("source"))

    test_code = {
        "user_intention": "Live Test",
        "language": data.get("language"),
        "prompt": data.get("source"),
    }
    runner(test_code)
    print("Sending Result")
    print(test_code)
    return test_code


# Setup flask server
app = Flask(__name__)


@app.route("/suggestions", methods=["GET", "POST"])
def ask_result():
    test_code = _process_request(request, _test_code_example)
    return json.dumps(test_code)


if __name__ == "__main__":
    app.run(port=5000, debug=True)
