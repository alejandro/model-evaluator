.PHONY: run-test
run-test:
	python3.11 results/evaluation.py --mode=""

.PHONY: run-generation
run-generation:
	python3.11 results/evaluation.py --mode="generation"

.PHONY: run-completion
run-completion:
	python3.11 results/evaluation.py --mode="completion"

.PHONY: run-vertex-gecko-generation
run-vertex-gecko-generation:
	python3.11 results/evaluation_vertex_gecko.py --mode="generation"

.PHONY: run-vertex-gecko-completion
run-vertex-gecko-completion:
	python3.11 results/evaluation_vertex_gecko.py --mode="completion"

.PHONY: inject-vertex-gecko
inject-vertex-gecko:
	python3.11 results/inject_gecko_into_results.py

python-format:
	isort ./eval-code-completion
	black -l 120 ./eval-code-completion
	autoflake --remove-all-unused-imports -i -r ./eval-code-completion