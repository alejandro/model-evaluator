const db = require('../db-config');

const thumbsUp = 'thumbsUp';
const thumbsDown = 'thumbsDown';
const notSure = 'notSure';
const validOptions = new Set([thumbsUp, thumbsDown, notSure]);
const invalidVoteMsg = 'Invalid vote option';
const requiredMsg = 'Model and language are required';

class Vote {
  static get thumbsUp() {
    return thumbsUp;
  }

  static get thumbsDown() {
    return thumbsDown;
  }

  static get notSure() {
    return notSure;
  }

  static get invalidVoteMsg() {
    return invalidVoteMsg;
  }

  static get requiredMsg() {
    return requiredMsg;
  }

  static get validOptions() {
    return validOptions;
  }

  static findById(promptResultId) {
    return db('votes').where({ prompt_result_id: promptResultId }).first();
  }

  constructor(promptResultId, params) {
    this.promptResultId = promptResultId;
    this.option = params.option;
    this.model = params.model;
    this.language = params.language;
    this.sessionId = params.sessionId; // optional for now
    this.validationError = null;
  }

  async save() {
    this._validate();

    if (this.validationError) {
      return;
    }

    const result = await db('votes').insert({
      prompt_result_id: this.promptResultId,
      option: this.option,
      model: this.model,
      language: this.language,
      session_id: this.session_id,
    });

    console.log(`${result.rowCount} vote inserted`);
  }

  _optionValid() {
    return !validOptions.has(this.option);
  }

  _validate() {
    if (!this.model || !this.language) {
      this.validationError = requiredMsg;
    }

    if (this._optionValid()) {
      this.validationError = invalidVoteMsg;
    }
  }
}

module.exports = Vote;
