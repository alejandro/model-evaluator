exports.up = function (knex) {
  return knex.schema.table('votes', (table) => {
    table.timestamps(true, true);
  });
};

exports.down = function (knex) {
  return knex.schema.table('votes', (table) => {
    table.dropTimestamps();
  });
};
