const getStorageKey = (version) => `ratedPrompts-${version}`;

const markPromptAsRated = (version, language, promptId) => {
  const prompts = getStoredPrompts(version);

  if (!prompts[language]) {
    prompts[language] = [];
  }

  prompts[language].push(promptId);

  setStoredPrompts(version, prompts);
};

const getRatedPrompts = (version, language) => {
  const prompts = getStoredPrompts(version);

  return prompts[language] || [];
};

const getStoredPrompts = (version) =>
  JSON.parse(localStorage.getItem(getStorageKey(version)) || '{}');

const setStoredPrompts = (version, prompts) =>
  localStorage.setItem(getStorageKey(version), JSON.stringify(prompts));

module.exports = {
  markPromptAsRated,
  getRatedPrompts,
};
