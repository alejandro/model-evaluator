const connection = {
  host: process.env.DB_HOST,
  user: process.env.DB_USER,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_NAME,
};

const migrations = {
  tableName: 'knex_migrations',
  directory: './migrations',
};

module.exports = {
  development: {
    client: 'postgres',
    connection,
    migrations,
  },
  test: {
    client: 'postgres',
    connection: { ...connection, ...{ database: process.env.DB_NAME_TEST } },
    migrations,
  },
  production: {
    client: 'postgres',
    connection,
    migrations,
    pool: {
      min: 2,
      max: 10,
    },
  },
};
