const { v4: uuidv4 } = require('uuid');

const db = require('__app__/db-config');

const Vote = require('__app__/models/vote');

describe('save', function () {
  let promptResultId;
  let voteOption;
  let model;
  let language;

  beforeAll(async () => {
    await db.migrate.latest();
  });

  afterAll(async () => {
    await db.migrate.rollback();
    await db.destroy();
  });

  beforeEach(() => {
    promptResultId = uuidv4();
    model = 'model-slug';
    language = 'language-slug';
    voteOption = Vote.notSure;
  });

  it('should have a validation error if a model is not provided', () => {
    const vote = new Vote(promptResultId, {
      option: voteOption,
      model: undefined,
      language,
    });

    vote.save();

    expect(vote.validationError).toBe(Vote.requiredMsg);
  });

  it('should have a validation error if the language is not provided', () => {
    const vote = new Vote(promptResultId, {
      option: voteOption,
      model,
      language: undefined,
    });

    vote.save();

    expect(vote.validationError).toBe(Vote.requiredMsg);
  });

  it('should have a validation error if an invalid vote option is provided', () => {
    const vote = new Vote(promptResultId, {
      option: 'not valid option',
      model,
      language,
    });

    vote.save();

    expect(vote.validationError).toBe(Vote.invalidVoteMsg);
  });

  it('should save valid votes', async () => {
    const sessionId = uuidv4();
    for (let i = 0; i < Vote.validOptions.length; i++) {
      const option = Vote.validOptions[i];
      const vote = await new Vote(
        promptResultId,
        option,
        model,
        language,
        sessionId
      );

      vote.save();

      const savedVote = await Vote.findById(promptResultId);

      expect(savedVote).toBeDefined();
      expect(savedVote.option).toBe(option);
      expect(savedVote.session_id).toBe(sessionId);
    }
  });
});
