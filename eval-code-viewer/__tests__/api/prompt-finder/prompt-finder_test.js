const path = require('path');
const PromptFinder = require('__app__/api/prompt-finder');

describe('promptDataService', () => {
  describe('v1', () => {
    const version = 'v1';
    describe('when instantiated correctly', () => {
      let promptDataService;
      const basePath = path.join(__dirname, '../test-data');
      const existingLanguageId = 'javascript';

      beforeEach(() => {
        promptDataService = new PromptFinder(basePath, version);
      });

      it('it should return correct data for existing language', () => {
        const promptData =
          promptDataService.getPromptsForLanguage(existingLanguageId);
        expect(promptData).toMatchSnapshot();
      });
    });

    describe('when instantiated incorrectly', () => {
      let promptDataService;
      const basePath = 'foobar';
      const existingLanguageId = 'javascript';

      beforeEach(() => {
        promptDataService = new PromptFinder(basePath, version);
      });

      it('it should return correct data for existing language', () => {
        const promptData =
          promptDataService.getPromptsForLanguage(existingLanguageId);
        expect(promptData.length).toBe(0);
      });
    });
  });
  describe('v2', () => {
    const version = 'v2';
    describe('when instantiated correctly', () => {
      let promptDataService;
      const basePath = path.join(__dirname, '../test-data');
      const existingLanguageId = 'javascript';

      beforeEach(() => {
        promptDataService = new PromptFinder(basePath, version);
      });

      it('it should return correct data for existing language', () => {
        const promptData =
          promptDataService.getPromptsForLanguage(existingLanguageId);
        expect(promptData).toMatchSnapshot();
      });
    });

    describe('when instantiated incorrectly', () => {
      let promptDataService;
      const basePath = 'foobar';
      const existingLanguageId = 'javascript';

      beforeEach(() => {
        promptDataService = new PromptFinder(basePath, version);
      });

      it('it should return correct data for existing language', () => {
        const promptData =
          promptDataService.getPromptsForLanguage(existingLanguageId);
        expect(promptData.length).toBe(0);
      });
    });
  });
});
