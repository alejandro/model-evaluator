const app = require('__app__/api/api');
const request = require('supertest');

const { v4: uuidv4 } = require('uuid');

const testPromptData = [
  {
    id: 'prompt-id',
    prompt: 'test prompt',
    generatedCode: 'test generated code',
  },
];

const Vote = require('__app__/models/vote');

jest.mock('__app__/db-config', () => {
  const mKnex = {
    insert: jest.fn().mockReturnValue({ rowCount: 1 }),
  };
  return jest.fn(() => mKnex);
});

jest.mock('__app__/api/prompt-finder', () => {
  return function () {
    return { getPromptsForLanguage: () => testPromptData };
  };
});

jest.mock('__app__/api/auth', () => {
  const actual = jest.requireActual('__app__/api/auth');
  return {
    ...actual,
    authenticated: (res, req, next) => next(),
  };
});

describe('/prompts/:languageId', () => {
  it('return data from promptDataService if languageId is correct', async () => {
    const languageId = 'javascript';
    const response = await request(app).get(`/prompts/${languageId}`);
    const expectedResponse = {
      promptsData: [
        {
          generatedCode: 'test generated code',
          id: 'prompt-id',
          prompt: 'test prompt',
        },
      ],
      promptsVersion: 'v1',
    };
    expect(response.status).toBe(200);
    expect(response.body).toEqual(expectedResponse);
  });

  it('return 400 status code and error message if languageId is correct', async () => {
    const languageId = 'whitespace';
    const response = await request(app).get(`/prompts/${languageId}`);

    expect(response.status).toBe(400);
    expect(response.body.error).toEqual('Invalid parameter: languageId');
  });
});

describe('/vote/:promptResultId', function () {
  let promptResultId;
  let voteOption;
  let model;
  let language;
  let completeApiUrl;

  beforeEach(() => {
    promptResultId = uuidv4();
    model = 'model-slug';
    language = 'language-slug';
    voteOption = Vote.notSure;

    completeApiUrl = `/vote/${promptResultId}?option=${voteOption}&model=${model}&language=${language}`;
  });

  it('should return a 400 if a model is not provided', async () => {
    const response = await request(app).post(
      `/vote/${promptResultId}?option=${voteOption}&language=${language}`
    );

    expect(response.status).toBe(400);
    expect(response.body.error).toBe(Vote.requiredMsg);
  });

  it('should return a 400 if a language is not provided', async () => {
    const response = await request(app).post(
      `/vote/${promptResultId}?option=${voteOption}&model=${model}&language=`
    );

    expect(response.status).toBe(400);
    expect(response.body.error).toBe(Vote.requiredMsg);
  });

  it('should return a 400 if an invalid vote option is provided', async () => {
    const invalidVoteOption = 'not_valid';
    const response = await request(app).post(
      completeApiUrl.replace(voteOption, invalidVoteOption)
    );

    expect(response.status).toBe(400);
    expect(response.body.error).toBe(Vote.invalidVoteMsg);
  });

  it('should submit a valid vote successfully', async () => {
    const response = await request(app).post(
      completeApiUrl.replace(voteOption, Vote.thumbsDown)
    );

    expect(response.status).toBe(200);
    expect(response.body.message).toBe('Vote recorded successfully');
  });
});
