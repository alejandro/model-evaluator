import json
from time import sleep, time
import datetime
import argparse
import numpy as np
import uuid
from vertexaicode_vertex_gecko import complete_vertex_gecko, Timer


models = ["vertex_gecko"]
languages = ["python", "golang", "c", "javascript"]
num_iterations = 10
batch_size = 5


def read_prompts(filename):
    with open(filename) as fp:
        return [line.strip() for line in fp]

def write_response_generation(json_string, language, count):
    with open(f"results/ERROR/generation/{language}/{count}.json", "w") as f:
        f.write(json_string)


def write_response_completion(json_string, language, count):
    with open(f"results/ERROR/completion/{language}/{count}.json", "w") as f:
        f.write(json_string)

def get_json_response(language, prompt_formatted, mode):
    
    t = Timer()
    t.start()
    res = complete_vertex_gecko(language, prompt_formatted)
    duration = t.stop()

    while not res:
        print("Failed to get result")
        sleep(5)
        print()
        res = complete_vertex_gecko(language, prompt_formatted)

    temporary_json_object = build_json_object(res, duration, mode)
    print("temp_json_object")
    print(temporary_json_object)

    return temporary_json_object

def build_json_object(res, duration):
    temporary_json_object = {}

    temporary_json_object["result"] = res
    temporary_json_object["duration"] = duration
    temporary_json_object["result_length"] = len(res)

    temporary_json_object = {
        'vertex_gecko': temporary_json_object
    }
    return temporary_json_object
    

def average_duration_into_json_blob(json_object, duration_dict) -> str:

    for model in models:
        if json_object[model]["result"]:
            filtered_list = [x for x in duration_dict[model] if x is not None]
            if len(filtered_list) > 0:
                json_object[model]["average_duration"] = np.mean(filtered_list)

    return json_object

def duration_dictionary_into_json_blob(json_object, duration_dict) -> str:

    for model in models:
        if json_object[model]["result"]:
            filtered_list = [x for x in duration_dict[model] if x is not None]
            if len(filtered_list) > 0:
                json_object[model]["raw_durations"] = filtered_list

    return json_object

def format_prompt(language, prompt, mode) -> str:
    if mode == "generation":
        prepend = "# " if language == "python" else "// "
        return f"{prepend}{prompt}"
    else:
        return f"{prompt}"

def collect_durations_per_prompt(temp_json_object, duration_dict):

    for model in models:
        try:
            duration_dict[model]
            try:
                duration_dict[f"{model}"] += [temp_json_object[model]["duration"]]
            except:
                duration_dict[f"{model}"] += [None]
        except:
            try:
                duration_dict[f"{model}"] = [temp_json_object[model]["duration"]]
            except:
                duration_dict[f"{model}"] = [None]

    return duration_dict

def check_for_blank_model_response(json_object, temp_json_object):

    for model in models:
        try:
            json_object[model]["result"]

            if json_object[model]["result"] == "":
                json_object[model] = temp_json_object[model]
        except:
            json_object[model] = temp_json_object[model]

    return json_object

def uuid_into_json_blob(json_object):

    for model in models:
        try:
            json_object[model]["uuid"] = str(uuid.uuid4())
        except:
            continue

    return json_object

def run_evaluation(language, prompt, prompt_num, mode):
    
    json_object = {}
    duration_dict = {}

    prompt_formatted = format_prompt(language, prompt)

    for _ in range(num_iterations):
        temp_json_object = get_json_response(language, prompt_formatted, mode)
        sleep(2)
        json_object = check_for_blank_model_response(json_object, temp_json_object)
        sleep(2)
        duration_dict = collect_durations_per_prompt(temp_json_object, duration_dict)
        sleep(3)

    print("Duration dictionary:")
    print(duration_dict)

    json_object = average_duration_into_json_blob(json_object, duration_dict)

    json_object = duration_dictionary_into_json_blob(json_object, duration_dict)

    json_object = uuid_into_json_blob(json_object)

    j_string = json.dumps(json_object, indent=2)

    if mode == "generation":
        write_response_generation(j_string, language, prompt_num)
    elif mode == "completion":
        write_response_completion(j_string, language, prompt_num)
    else:
        print("this mode was not an option")
        exit()

def is_succeeded(res):
    j_object = json.loads(res)

    try:
        _ = j_object["anthropic"]
    except:
        return False
    
    return True

def batch_prompts(prompts):
    num_batches = len(prompts) // batch_size

    if len(prompts) % batch_size != 0:
        num_batches += 1

    batches = []
    for i in range(num_batches):
        batches.append(prompts[i * batch_size:(i + 1) * batch_size])

    return batches

def choose_prompt_file(language, mode):
    if mode == "generation":
        print("using generation prompts")
        prompts_file = f"results/v1/prompts/generation/PROMPTS_{language}.txt"
    elif mode == "completion":
        print("using completion prompts")
        prompts_file = f"results/v1/prompts/completion/PROMPTS_{language}.txt"
    else:
        print("using default prompts")
        prompts_file = "results/v1/PROMPTS_raw.txt"

    return prompts_file


def main():

    parser = argparse.ArgumentParser()
    parser.add_argument('--mode', help='Indicates what training is to be done')
    args = parser.parse_args()
    mode = args.mode

    start_time = time()

    for language in languages:

        prompts_file = choose_prompt_file(language, mode)
        prompts = read_prompts(prompts_file)
        prompts_batched = batch_prompts(prompts)
        prompt_num = 0

        for prompts in prompts_batched:
            
            for prompt in prompts:
                prompt_num += 1
                
                if mode == "generation" or mode == "completion":
                    t = Timer()
                    t.start()
                    run_evaluation(language, prompt, prompt_num, mode)
                else:
                    print("Mode set to default, no testing will occcur")
                    exit()
                
                sleep(3)
            
            print("Resting before next batch...")
            sleep(10)

    stop_time = time()

    duration = datetime.timedelta(seconds=(stop_time - start_time))
    print("Duration of testing: ", duration)


if __name__ == "__main__":
    main()