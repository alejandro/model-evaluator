from google.cloud import aiplatform
from google.protobuf import json_format
from google.protobuf.struct_pb2 import Value
import time


token_target_creation = 256
token_target_completion = 128

google_project = "unreview-poc-390200e5"


def vertexai_predict(
    model: str,
    input: str,
    parameters: str,
    location: str = "us-central1",
):
    api_endpoint = "us-central1-aiplatform.googleapis.com"
    endpoint = "projects/" + google_project + "/locations/us-central1/publishers/google/models/" + model
    # The AI Platform services require regional API endpoints.
    client_options = {"api_endpoint": api_endpoint}
    # Initialize client that will be used to create and send requests.
    # This client only needs to be created once, and can be reused for multiple requests.
    client = aiplatform.gapic.PredictionServiceClient(client_options=client_options)
    instance_dict = input
    instance = json_format.ParseDict(instance_dict, Value())
    instances = [instance]
    parameters_dict = parameters
    parameters = json_format.ParseDict(parameters_dict, Value())
    response = client.predict(endpoint=endpoint, instances=instances, parameters=parameters)
    print("response ")
    predictions = response.predictions
    result_predictions = []
    for prediction in predictions:
        print(" prediction:", dict(prediction))
        result_predictions.append(dict(prediction))
    return result_predictions



def complete_vertex_code_gecko(prefix, suffix):
    GECKO_MAXIMUM_TOKEN_OUTPUT = 64

    predictions = vertexai_predict(
        "code-gecko@001",
        {"prefix": prefix, "suffix": suffix},
        {"temperature": 0.2, "maxOutputTokens": GECKO_MAXIMUM_TOKEN_OUTPUT},
        "us-central1",
    )

    if predictions:
        prediction_text = predictions[0]["content"]
        return prediction_text
    else:
        return "ERROR"


def complete_vertex_gecko(language, code):
    code_parts = code.split("[[CURSOR]]")
    prefix = code_parts[0]
    suffix = code_parts[1] if len(code_parts) > 1 else ""

    return complete_vertex_code_gecko(prefix, suffix)

class TimerError(Exception):
    """A custom exception used to report errors in use of Timer class"""

class Timer:
    def __init__(self):
        self._start_time = None

    def start(self):
        """Start a new timer"""
        if self._start_time is not None:
            raise TimerError(f"Timer is running. Use .stop() to stop it")

        self._start_time = time.perf_counter()

    def stop(self):
        """Stop the timer, and report the elapsed time"""
        if self._start_time is None:
            raise TimerError(f"Timer is not running. Use .start() to start it")

        elapsed_time = time.perf_counter() - self._start_time
        self._start_time = None
        print(f"Elapsed time: {elapsed_time:0.4f} seconds")
        return elapsed_time